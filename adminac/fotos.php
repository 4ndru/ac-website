<?php
   session_start();

   if (!isset($_SESSION["idusuario"]) || $_SESSION["idusuario"] == null) {
      print "<script>alert(\"Favor de iniciar sesión para poder entrar.\");window.location='../index.php';</script>";
   }
?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Fotos - Administración Adriana Crespo Fotografía</title>

		<!-- CSS -->
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="../assets/css/styleAd.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
		<link href="https://fonts.googleapis.com/css?family=Cormorant+SC:300,400,500,600,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:300,400,600,700" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

		<link rel="icon" type="image/png" href="../assets/img/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="../assets/img/favicon-16x16.png" sizes="16x16" />

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<div class="wrapper">
			<!-- Sidebar -->
			<nav id="sidebar">
				<div id="dismiss">
					<i class="glyphicon glyphicon-arrow-left"></i>
				</div>

				<div class="sidebar-header">
					<h3>Adriana Crespo</h3>
				</div>

				<ul class="list-unstyled components">
					<p class="center"><b>Administración</b></p>
					<li>
						<a href="admin.php">Información</a>
					</li>
					<li class="active">
						<a href="fotos.php">Fotos</a>
					</li>
					<li class="visible-xs"><a href="../php/logout.php">Cerrar sesión</a></li>
				</ul>
			</nav>

			<!-- MAIN -->
			<div id="content">
				<nav class="navbar navbar-default">
					<div class="container-fluid">

						<div class="navbar-header">
							<button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
								<i class="fas fa-bars"></i>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                       <li><a href="../php/logout.php">Cerrar sesión</a></li>
                     </ul>
                  </div>
					</div>
				</nav>

				<!-- MUESTREO DE FOTOS ACTUALES DENTRO DE LA BD -->
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="center">FOTOS MOSTRADAS</h1>
							
							<?php 
								include_once ('../php/db.php');

								$sql = "SELECT imagen FROM fotos";
								$query = mysqli_query($conn, $sql);
								if (!$query) {
									$error = die("SQL Error: " . mysqli_error($conn));
									echo "<script>alert(\"$error\");</script>";
								}

								//Hacer un array para obtener las rutas de las imagenes
								while ($r = mysqli_fetch_row($query)) {
								 	// $boda1 = $r['imagen'];
								 	// $boda2 = $r['imagen'];
									// $boda3 = $r['imagen'];
									// $boda4 = $r=['imagen'];

									$fotos_array = array(
										$r = ['imagen']
									);
								}

								echo $fotos_array[0];
							?>
							
							<div class="col-sm-3">
								<img src="<?php echo $boda1;?>" alt="Boda1" class="img-responsive img-prev">
							</div>
							<div class="col-sm-3">
								<img src="<?php echo $boda2;?>" alt="Boda2" class="img-responsive img-prev">
							</div>
							<div class="col-sm-3">
								<img src="<?php echo $boda3;?>" alt="Boda3" class="img-responsive img-prev">
							</div>
							<div class="col-sm-3">
								<img src="<?php echo $boda3;?>" alt="Boda3" class="img-responsive img-prev">
							</div>
						</div>
					</div>
				</div>


				<div class="container">
					<div class="row">
						<div class="col-md-12">
                     <form enctype="multipart/form-data" action="../php/upload.php" method="POST">
								<p>Selecciona una opción</p>

								<div class="select">
									<select name="fotosList" id="select">
										<option value="null">Selecciona...</option>
										<option value="boda1">Boda - Foto #1</option>
										<option value="boda2">Boda - Foto #2</option>
										<option value="boda3">Boda - Foto #3</option>
										<option value="boda4">Boda - Foto #4</option>
										<option value="preg1">Pregnancy - Foto #1</option>
										<option value="preg2">Pregnancy - Foto #2</option>
										<option value="preg3">Pregnancy - Foto #3</option>
										<option value="preg4">Pregnancy - Foto #4</option>
										<option value="xv1">XV Años - Foto #1</option>
										<option value="xv2">XV Años - Foto #2</option>
										<option value="xv3">XV Años - Foto #3</option>
										<option value="xv4">XV Años - Foto #4</option>
									</select>
								</div>

								<input type="file" name="uploaded_file"></input><br />
								<input type="submit" value="Subir"></input>
							</form>
						</div>
					</div>
				</div>
			</div>
		
		<div class="overlay"></div>

		<!-- JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript">
         $(document).ready(function () {

				$("#sidebar").mCustomScrollbar({
					theme: "minimal"
				});

				// when opening the sidebar
				$('#sidebarCollapse').on('click', function () {
					// open sidebar
					$('#sidebar').addClass('active');
					// fade in the overlay
					$('.overlay').fadeIn();
					$('.collapse.in').toggleClass('in');
					$('a[aria-expanded=true]').attr('aria-expanded', 'false');
				});

				// if dismiss or overlay was clicked
				$('#dismiss, .overlay').on('click', function () {
				// hide the sidebar
					$('#sidebar').removeClass('active');
					// fade out the overlay
					$('.overlay').fadeOut();
				});
			});
		</script>
	</body>
</html>