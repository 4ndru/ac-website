<?php
	session_start();

   if (!isset($_SESSION["idusuario"]) || $_SESSION["idusuario"] == null) {
      print "<script>alert(\"Favor de iniciar sesión para poder entrar.\");window.location='../index.php';</script>";
   }
?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Administración - Administración Adriana Crespo Fotografía</title>

		<!-- CSS -->
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="../assets/css/styleAd.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
		<link href="https://fonts.googleapis.com/css?family=Cormorant+SC:300,400,500,600,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:300,400,600,700" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

		<link rel="icon" type="image/png" href="../assets/img/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="../assets/img/favicon-16x16.png" sizes="16x16" />

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<div class="wrapper">
			<!-- SIDEBAR -->
			<nav id="sidebar">
				<div id="dismiss">
					<i class="glyphicon glyphicon-arrow-left"></i>
				</div>

				<div class="sidebar-header">
					<h3>Adriana Crespo</h3>
				</div>

				<ul class="list-unstyled components">
					<p class="center"><b>Administración</b></p>
					<li>
						<a href="#">Información</a>
					</li>
					<li>
						<a href="fotos.php">Fotos</a>
					</li>
					<li class="visible-xs"><a href="../php/logout.php">Cerrar sesión</a></li>
				</ul>
			</nav>

			<!-- MAIN -->
			<div id="content">

				<!-- NAVBAR -->
				<nav class="navbar navbar-default">
					<div class="container-fluid">

						<div class="navbar-header">
							<button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
								<i class="fas fa-bars"></i>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                       <li><a href="../php/logout.php">Cerrar sesión</a></li>
                     </ul>
                  </div>
					</div>
				</nav>

				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<h1 class="center">Información presentada</h1>

							<h3>Página inicial (inicio):</h3>

							<?php
								include_once ('../php/db.php');

								$sql = "SELECT * FROM info";
								$query = mysqli_query($conn, $sql);
								if (!$query) {
									header('Location: ../error.php');
									print("SQL Error: " . mysqli_error($conn));
								}

								while ($row = mysqli_fetch_array($query)) {
									$inicio = $row['inicio'];
									$acerca1 = $row['acerca1'];
									$acerca2 = $row['acerca2'];
									$email = $row['email'];
									$telefono = $row['telefono'];
									break;
								}
								echo "<p class='jstf'>$inicio</p>";
							?>
							<br><h3>Acerca:</h3>
							<?php
								echo "<p class='jstf'>$acerca1</p>";
								echo "<h5>[Foto]</h5>";
								echo "<p class='jstf'>$acerca2</p>";
							?>

							<div class="line"></div>

							<h1 class="center">Modificar información</h1> <br>

							<h3 class="center">Información de la página</h3>

							<form action="../php/update_info.php" method="post">
								<div class="col-md-12">
									<div class="form-group">
										<textarea class="form-control textarea" rows="1" name="inicio" placeholder="Inicio" required><?php print $inicio; ?></textarea>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<textarea class="form-control textarea" rows="1" name="acerca1" placeholder="Acerca (Primera parte)" required><?php print $acerca1; ?></textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<textarea class="form-control textarea" rows="1" name="acerca2" placeholder="Acerca (Segunda parte)" required><?php print $acerca2; ?></textarea>
									</div>
								</div>

								<br><h3 class="center">Información personal</h3>

								<div class="col-sm-6">
									<div class="form-group">
										<textarea class="form-control textarea" rows="0" name="email" placeholder="Correo electronico" required><?php print $email; ?></textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<textarea class="form-control textarea" rows="0" name="telefono" placeholder="Telefono" required><?php print $telefono; ?></textarea>
									</div>
								</div>

								<button type="submit" class="btn main-btn center">Modificar</button>
							</form>

						</div>
					</div>
				</div>
			</div>
		
		<div class="overlay"></div>

		<!-- JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript">
         $(document).ready(function () {

				$("#sidebar").mCustomScrollbar({
					theme: "minimal"
				});

				// when opening the sidebar
				$('#sidebarCollapse').on('click', function () {
					// open sidebar
					$('#sidebar').addClass('active');
					// fade in the overlay
					$('.overlay').fadeIn();
					$('.collapse.in').toggleClass('in');
					$('a[aria-expanded=true]').attr('aria-expanded', 'false');
				});

				// if dismiss or overlay was clicked
				$('#dismiss, .overlay').on('click', function () {
				// hide the sidebar
					$('#sidebar').removeClass('active');
					// fade out the overlay
					$('.overlay').fadeOut();
				});
			});
   	</script>
	</body>
</html>