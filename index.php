<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Adriana Crespo Fotografía.</title>

		<!-- CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/styles.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
		<link href="https://fonts.googleapis.com/css?family=Cormorant+SC:300,400,500,600,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:300,400,600,700" rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">

		<link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16" />

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- LOADER -->
		<div class="se-pre-con"></div>

		<!-- NAVBAR -->
		<section id="logo">
			<img src="assets/img/logo.png" alt="logo" class="center img-responsive">
		</section>

		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Menú</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="activo"><a href="index.php">Inicio</a></li>
						<li><a href="acerca.php">Acerca</a></li>
						<li><a href="portfolio.php">Galería</a></li>
						<li><a href="contacto.php">Contacto</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<!-- CAROUSEL -->
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			</ol>

			<div class="carousel-inner">
				<div class="item active">
					<div class="box">
						<img src="assets/img/img1.jpg" width="100%" height="100%" alt="Imagen 1" class="img-responsive">
					</div>
				</div>
				<div class="item">
					<div class="box">
						<img src="assets/img/img2.jpg" width="100%" alt="Imagen 2" class="img-responsive">
					</div>
				</div>
				<div class="item">
					<div class="box">
						<img src="assets/img/img3.jpg" width="100%" alt="Imagen 3" class="img-responsive">
					</div>
				</div>
			</div>

			<!-- Controles -->
			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>

		<!-- MAIN -->
		<section id="bienvenido">
			<div class="container">
				<div class="row">
					<div class="col-md-12 center">
						<h2>Bienvenido a Adriana Crespo Fotografía</h2>

						<?php
							include 'php/db.php';

							$sql = "SELECT * FROM info";
							$query = mysqli_query($conn, $sql);
							if (!$query) {
								$error = die("SQL Error: " . mysqli_error($conn));
								echo "<script>alert(\"$error\");</script>";
							}

							while ($row = mysqli_fetch_array($query)) {
								echo "<p style='font-size: 18px'>" . $row['inicio'] . "</p>";
							}
               	?>

						<!-- <p style="font-size: 18px">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corrupti, nobis temporibus ut quasi nam odit aliquid at dolor labore repudiandae? Rem eos deleniti voluptate nobis ab libero eum voluptatum assumenda?</p>

						<p style="font-size: 18px">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae ipsa earum molestiae totam laboriosam, consequatur tempora veritatis eos consequuntur aperiam harum, veniam voluptatibus quae. Mollitia saepe sapiente nobis asperiores corporis.</p> -->
					</div>
				</div>
			</div>
		</section>

		<section id="trabajos">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1 class="center">Mis Trabajos</h1>
					</div>

					<div class="col-md-4">
						<a href="assets/img/thumb1.jpg" data-fancybox="gallery" data-caption=" ">
							<img src="assets/img/thumb1.jpg" alt="Thumb1" class="img-responsive">
						</a>
					</div>
					<div class="col-md-4">
						<a href="assets/img/thumb2.jpg" data-fancybox="gallery" data-caption=" ">
							<img src="assets/img/thumb2.jpg" alt="Thumb1" class="img-responsive">
						</a>
					</div>
					<div class="col-md-4">
						<a href="assets/img/thumb3.jpg" data-fancybox="gallery" data-caption=" ">
							<img src="assets/img/thumb3.jpg" alt="Thumb1" class="img-responsive">
						</a>
					</div>

					<div class="col-sm-12 center"> <br><br>
						<a href="portfolio.php" class="center">Ver más</a>
					</div>
				</div>
			</div>
		</section>

		<section id="contacto">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1 class="center">Contacto</h1>

						<form role="form" id="contact-form" class="contact-form" action="php/contactar.php">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input type="text" class="form-control" name="nombre" autocomplete="off" id="nombre" placeholder="Nombre" required>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" name="telefono" autocomplete="off" id="telefono" placeholder="Telefono/celular" required>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="email" class="form-control" name="usr_email" autocomplete="off" id="usr_email" placeholder="Correo electronico" required>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<textarea class="form-control textarea" rows="3" name="mensaje" id="mensaje" placeholder="Mensaje" required></textarea>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12 center">
									<button type="submit" class="btn main-btn">Contactar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		
		<!-- FOOTER -->
		<footer class="center">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h4 style="color: #fff;">&copy; 2018 ~ Adriana Crespo Fotografía</h4>
						<h6 style="color: #fff;">Desarrollado por: <b>SA</b> &dash; 130418</h6>
					</div>
				</div>
			</div>
		</footer>

		<!-- JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
		<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
		<script>
			$(window).load(function() {
				// Animate loader off screen
				$(".se-pre-con").fadeOut("slow");;
			});
		</script>
	</body>
</html>