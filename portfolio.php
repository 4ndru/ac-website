<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Galería - Adriana Crespo Fotografía.</title>

      <!-- CSS -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/styles.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
      <link href="https://fonts.googleapis.com/css?family=Cormorant+SC:300,400,500,600,700" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:300,400,600,700" rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
		
		<link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16" />

      <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
		<!-- LOADER -->
		<div class="se-pre-con"></div>

      <!-- NAVBAR -->
		<section id="logo">
			<img src="assets/img/logo.png" alt="logo" class="center img-responsive">
		</section>
   
      <nav class="navbar navbar-default" role="navigation">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Menú</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
            </div>
   
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav">
                  <li><a href="index.php">Inicio</a></li>
                  <li><a href="acerca.php">Acerca</a></li>
                  <li class="activo"><a href="portfolio.php">Galería</a></li>
                  <li><a href="contacto.php">Contacto</a></li>
               </ul>
            </div>
         </div>
      </nav>

      <!-- SEPARATOR -->
      <div class="separator" style="background: url('assets/img/separator2.jpg') no-repeat center center;">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <h1 class="center" style="color: #333;">Galería</h1>
               </div>
            </div>
         </div>
      </div>

      <!-- PORTFOLIO -->
      <section id="info">
         <div class="container">
               <div class="row">
						<h3 class="center">BODAS</h3> <br>
                  <div class="col-sm-3">
                     <a href="assets/img/port1.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port1.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
                  <div class="col-sm-3">
                     <a href="assets/img/port2.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port2.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
                  <div class="col-sm-3">
                     <a href="assets/img/port3.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port3.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
                  <div class="col-sm-3">
                     <a href="assets/img/port4.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port4.jpg" alt="Thumb1" class="img-responsive">
                     </a>
							<br><br>
                  </div>

						<br><br><h3 class="center">PREGNANCY</h3> <br>
                  <div class="col-sm-3">
                     <a href="assets/img/port5.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port5.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
                  <div class="col-sm-3">
                     <a href="assets/img/port6.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port6.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
                  <div class="col-sm-3">
                     <a href="assets/img/port7.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port7.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
                  <div class="col-sm-3">
                     <a href="assets/img/port8.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port8.jpg" alt="Thumb1" class="img-responsive">
                     </a>
							<br><br>
                  </div>

						<h3 class="center">XV AÑOS</h3> <br>
						<div class="col-sm-3">
                     <a href="assets/img/port5.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port5.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
                  <div class="col-sm-3">
                     <a href="assets/img/port6.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port6.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
                  <div class="col-sm-3">
                     <a href="assets/img/port7.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port7.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
                  <div class="col-sm-3">
                     <a href="assets/img/port8.jpg" data-fancybox="gallery" data-caption=" ">
                        <img src="assets/img/port8.jpg" alt="Thumb1" class="img-responsive">
                     </a>
                  </div>
               </div>
            </div>
      </section>
      
      <!-- FOOTER -->
		<footer class="center">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h4 style="color: #fff;">&copy; 2018 ~ Adriana Crespo Fotografía</h4>
                  <h6 style="color: #fff;">Desarrollado por: <b>SA</b> &dash; 130418</h6>
               </div>
            </div>
         </div>
      </footer>

      <!-- JS -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
		<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
      <script>
			$(window).load(function() {
         	// Animate loader off screen
         	$(".se-pre-con").fadeOut("slow");;
         });
      </script>
   </body>
</html>