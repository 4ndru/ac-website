<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Iniciar Sesión - AC Administración</title>

		<!-- CSS -->
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <link rel="stylesheet" href="assets/css/styleLogin.css">
      <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16" />

      <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
		<!-- LOADER -->
		<div class="se-pre-con"></div>

		<!-- MAIN -->
		<section class="login-block">
			<div class="container">
				<div class="row">
					<div class="col-md-4 login-sec">
						<h2 class="text-center">Iniciar sesión</h2>

						<form class="login-form" action="php/login.php" role="form" id="contact-form" class="contact-form" method="post">
							<div class="form-group">
								<label for="exampleInputEmail1" class="text-uppercase">Usuario</label>
								<input type="text" class="form-control" name="usuario" autocomplete="off" id="usuario" placeholder="" required>

							</div>
							<div class="form-group">
								<label for="exampleInputPassword1" class="text-uppercase">Contraseña</label>
								<input type="password" class="form-control" name="pass" autocomplete="off" id="pass" placeholder="" required>
							</div>

							<div class="form-check">
								<button type="submit" class="btn btn-login float-right">Ingresar</button>
							</div>
						</form>

						<button onclick="window.location.href='index.php'" class="btn btn-login float-left">Regresar</button>

					</div>
					<div class="col-md-8 banner-sec">
						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner" role="listbox">
								<div class="carousel-item active">
									<img class="d-block img-fluid" src="https://scontent-dfw5-2.xx.fbcdn.net/v/t1.0-9/32557105_714542128669717_2374190112285130752_o.jpg?_nc_cat=0&oh=eb2fc750c93e2346e7f9b55cee7e7e73&oe=5B89BCAE" alt="First slide">
								</div>
								<div class="carousel-item">
									<img class="d-block img-fluid" src="https://scontent-dfw5-2.xx.fbcdn.net/v/t31.0-8/28516682_670670099723587_2284996920477122762_o.jpg?_nc_cat=0&oh=99cdea854e1d1eed0444c82bd4752d4c&oe=5B89C952" alt="First slide">
								</div>
								<div class="carousel-item">
									<img class="d-block img-fluid" src="https://scontent-dfw5-2.xx.fbcdn.net/v/t31.0-8/25440053_627297060727558_6169097256728855615_o.jpg?_nc_cat=0&oh=8a969041ba8bf8b0e99ea74e85d5b1e2&oe=5B956EB3" alt="First slide">
								</div>
							</div>
						</div>
					</div>
				</div>
		</section>
      
      <!-- JS -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
      <script>
			$(window).load(function() {
         	// Animate loader off screen
         	$(".se-pre-con").fadeOut("slow");;
         });
      </script>
   </body>
</html>